#!/usr/bin/env python3
# Authors:  David Vodák <xvodak05@stud.fit.vutbr.cz>
#           Adam Freiberg <xfreib00@stud.fit.vutbr.cz>

from sys import path

path.append("./src")

from kachlarna import Kachlarna

if __name__ == "__main__":
	try:
		# create kachlarna
		game = Kachlarna(1800, 800)
		# run till the end
		game.run()
	except KeyboardInterrupt:
		pass
