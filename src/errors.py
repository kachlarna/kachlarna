class Unexpected_Error(Exception):

	def __init__(self, *args):
		if args:
			self.msg = args[0]
		else:
			self.msg = None

	def __str__(self):
		if self.msg:
			return "Error: {0}".format(self.msg)
		else:
			return "Error: An unexpected error has occured, please restart the game"