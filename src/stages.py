#!/usr/bin/env python3
# Authors:  David Vodák     <xvodak05@stud.fit.vutbr.cz>
#           Adam Freiberg   <xfreib00@stud.fit.vutbr.cz>
#           Daniel Kondys   <dankondys@gmail.com>
#           Jakub Mastík    <jakubmastik35@gmail.com>
#           Šimon Spielmann <simonsp37@gmail.com>

import pygame
from menu import textMenu, boxMenu
from game_classes import text, TheDan, kachle, lava, image, dan

class stage_template():
	"""Template for stages, every stage inherits from this class"""

	def __init__(self, screen, max_x, max_y):
		"""Constructor for stages

		Child classes should call this constructor in theirs.

		Args:
			screen (Surface): Surface to draw
			max_x (int): Number of pixels in the x axis
			max_y (int): Number of pixels in the y axis
		"""
		self.screen = screen
		self.max_x = max_x
		self.max_y = max_y

	def render_and_handle(self):
		""" Function which will be called in the infinite loop

		Childs should use this method to handle the inputs (keyboard, mouse.. )
		and create output (draw to the window)
		"""	
		pass


class menu_1(stage_template):
	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y) 
		self.color = pygame.Color(100, 100, 100)
		self.items = ["Play intro", "Play spielgame", "Do nothing xd", "Exit"]
		self.menu = boxMenu(self.screen, self.items)

	def process_textval(self, textval):
		if textval == self.items[0]:
			return 'stage_1'
		elif textval == self.items[1]:
			return 'stage_2'
		elif textval == self.items[2]:
			print("I actualy did something ^⨀ᴥ⨀^")
			return 'menu_1'
		elif textval == self.items[3]:
			exit()
		else:
			return 'stage_1'

	def render_and_handle(self):
		pygame.time.delay(50)
		ret = 'menu_1'
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_DOWN:
					self.menu.move_down()
				elif event.key == pygame.K_UP:
					self.menu.move_up()
				elif event.key == pygame.K_RETURN:
					ret = self.process_textval( \
						self.menu.select().textval)

		self.screen.fill(self.color)
		self.menu.render()
		return ret

class stage_1(stage_template):

	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y)
		self.color = pygame.Color(50, 60, 210)

		# list for all objects, that are going to be rendered
		self.renderables = []

		# create normal kachles
		self.kachles = []
		kachle_size = 50
		for i in range(int(max_x/kachle_size)):
			tmp_kachle = kachle(screen, i*kachle_size + kachle_size/2, \
				max_y - kachle_size/2, kachle_size)
			self.kachles.append(tmp_kachle)
			self.renderables.append(tmp_kachle)

		# create super kachle
		super_kachle = kachle(screen, kachle_size, \
			max_y - kachle_size * 2, kachle_size * 2)
		self.kachles.append(super_kachle)
		self.renderables.append(super_kachle)

		# create giga kachle
		self.giga_kachle = kachle(screen, max_x - kachle_size * 2, \
				max_y - kachle_size * 3, kachle_size * 4)
		self.kachles.append(self.giga_kachle)
		self.renderables.append(self.giga_kachle)

		# create jump kachles
		y = 700
		x_step = kachle_size*5
		for i in range(5):
			y -= kachle_size*2
			x_step += 10
			tmp_kachle = kachle(screen, 200 + i*x_step, y, kachle_size)
			self.kachles.append(tmp_kachle)
			self.renderables.append(tmp_kachle)

		# create lava
		lava_width  = 50
		lava_length = max_x - kachle_size * 6
		self.lava = lava(screen, max_x/2 - kachle_size, \
			max_y - (kachle_size + lava_width/2), lava_length, lava_width)
		self.renderables.append(self.lava)

		# create finish mark
		finish_mark = text(screen, 60, (240, 240, 240), "Finish", \
			max_x - 100, max_y - 300)
		self.renderables.append(finish_mark)

		#create dan
		self.dan = TheDan(screen, 50, 300, self.kachles, \
				self.lava, self.giga_kachle)
		self.renderables.append(self.dan)

	def reset(self):
		# set dan's position to default place
		self.dan.move( 50 - self.dan.x, -self.dan.y)

	def render_and_handle(self):
		pygame.time.delay(10)
		# handle
		for event in pygame.event.get():
			# quit
			if event.type == pygame.QUIT:
				exit()
			# jump
			if event.type == pygame.KEYDOWN and \
			   event.key == pygame.K_SPACE:
				self.dan.jump()

		keys = pygame.key.get_pressed()
		if keys[pygame.K_LEFT]:
			self.dan.run_left()
		elif keys[pygame.K_RIGHT]:
			self.dan.run_right()

		# react
		ret = self.dan.live()
		if ret == "l":
			self.reset()
			return 'stage_loss'
		elif ret == "w":
			self.reset()
			return 'stage_win'
		elif ret == "v":
			self.reset()
			return 'stage_void'

		# render
		self.screen.fill(self.color)
		for renderable in self.renderables:
			renderable.render(self.max_x/2 - self.dan.x, \
					self.max_y/3 - self.dan.y)
		return 'stage_1'

class stage_2(stage_template):

	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y)
		self.color = pygame.Color(50, 60, 210)

		# list for all objects, that are going to be rendered
		self.renderables = []

		# create normal kachles
		self.kachles = []
		kachle_size = 100
				
		# create platform kachle
		for k in range(6):
			platform_kachle = kachle(screen, k*(kachle_size + 100) + 100, \
				2700 - 2*k*kachle_size, kachle_size/2)
			self.kachles.append(platform_kachle)
			self.renderables.append(platform_kachle)
			
		for k in range(5):
			platform_kachle = kachle(screen, 1100 - k*(kachle_size + 100), \
				1200 - 2*k*kachle_size, kachle_size/2)
			self.kachles.append(platform_kachle)
			self.renderables.append(platform_kachle)
		
		for k in range(6):
			platform_kachle = kachle(screen, k*(kachle_size + 100) + 200, \
				0 - 2*k*kachle_size, kachle_size/2)
			self.kachles.append(platform_kachle)
			self.renderables.append(platform_kachle)

		# create start kachle
		start_kachle = kachle(screen, kachle_size, \
			2800 - kachle_size / 2, kachle_size * 2)
		self.kachles.append(start_kachle)
		self.renderables.append(start_kachle)

		# create big platform kachle 1
		big_platform_kachle_1 = kachle(screen, 1450, \
			1600 - kachle_size / 2, kachle_size * 2)
		self.kachles.append(big_platform_kachle_1)
		self.renderables.append(big_platform_kachle_1)

		# create big platform kachle 2
		big_platform_kachle_2 = kachle(screen, kachle_size - 100, \
			200 - kachle_size / 2, kachle_size * 2)
		self.kachles.append(big_platform_kachle_2)
		self.renderables.append(big_platform_kachle_2)

		# create win kachle
		self.win_kachle = kachle(screen, 1500, (-1200), kachle_size * 2)
		self.kachles.append(self.win_kachle)
		self.renderables.append(self.win_kachle)

		# create finish mark
		finish_mark = text(screen, 60, (240, 240, 240), "Finish", \
			1500, (-1200))
		self.renderables.append(finish_mark)

		# create lava
		lava_width  = 1000
		lava_length = 1000 * 20
		self.lava = lava(screen, kachle_size/2 + 100, \
			3600 - kachle_size * 2, lava_length, lava_width)
		self.renderables.append(self.lava)

		# Ddan
		self.dan = TheDan(screen, 50, 2500, self.kachles, \
				self.lava, self.win_kachle)
		self.renderables.append(self.dan)

	def reset(self):
		# set dan's position to default place
		self.dan.move( 50 - self.dan.x, 2500 - self.dan.y)
		self.lava.get_back()

	def render_and_handle(self):
		pygame.time.delay(10)
		# handle
		for event in pygame.event.get():
			# quit
			if event.type == pygame.QUIT:
				exit()
			# jump
			if event.type == pygame.KEYDOWN and \
			   event.key == pygame.K_SPACE:
				self.dan.jump()

		keys = pygame.key.get_pressed()
		if keys[pygame.K_LEFT]:
			self.dan.run_left()
		elif keys[pygame.K_RIGHT]:
			self.dan.run_right()

		self.lava.grow()

		# react
		ret = self.dan.live()
		if ret == "l":
			self.reset()
			return 'stage_loss'
		elif ret == "w":
			self.reset()
			return 'stage_win'
		elif ret == "v":
			self.reset()
			return 'stage_void'

		# render
		self.screen.fill(self.color)
		for renderable in self.renderables:
			renderable.render(self.max_x/2 - self.dan.x, \
					self.max_y/3 - self.dan.y)
		return 'stage_2'

class stage_void(stage_template):

	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y)
		self.r = 50
		self.g = 60
		self.b = 210
		
		self.dan = dan(screen, max_x/2, max_y/3)
	
	def render_and_handle(self):
		# handle
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RETURN:
					return 'stage_loss'
		# render
		pygame.time.delay(50)
		self.screen.fill((self.r, self.g, self.b))
		self.dan.render()
		if self.r > 0:
			self.r -= 1
		if self.g > 0:
			self.g -= 1
		if self.b > 0:
			self.b -= 1
		
		if self.r < 1 and self.g < 1 and self.b < 1:
			pygame.time.delay(1000)
			return 'stage_loss'

		return 'stage_void'	


class stage_win(stage_template):
	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y)
		self.f_pressed = False
		self.color = pygame.Color(0, 0, 128)
		# create  mark
		self.Win_mark = text(screen, 80, (240, 240, 240), "You have won!", \
			max_x/2, max_y/2)
		self.Continue_mark = text(screen, 60, (240, 240, 240), "Press Enter to continue.", \
			max_x/2, (max_y/2)+80)
		self.Leave_mark = text(screen, 60, (240, 240, 240), "Press Escape to leave.", \
			max_x/2, (max_y/2)+160)
		self.reddit_picovinka = text(screen, 80, (240, 240, 240), "Press F to pay respect!", \
			max_x/2, max_y/2)


	def render_and_handle(self):
		pygame.time.delay(50)
		self.screen.fill(self.color)
		for event in pygame.event.get():
			# quit
			if event.type == pygame.QUIT:
				exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RETURN:
					return 'menu_1'
				elif event.key == pygame.K_ESCAPE:
					exit()
				elif event.key == pygame.K_f:
					self.f_pressed = True		
		if self.f_pressed:
			self.reddit_picovinka.render() 
		else:
			self.Win_mark.render()
			self.Continue_mark.render()
			self.Leave_mark.render()	
		return 'stage_win'

class stage_loss(stage_template):
	def __init__(self, screen, max_x, max_y):
		super().__init__(screen, max_x, max_y)
		self.color = pygame.Color(139, 0, 0)
		self.Loss_mark = text(screen, 80, (240, 240, 240), "You have lost!", \
			max_x/2, max_y/2+20)
		self.Continue_mark = text(screen, 60, (240, 240, 240), "Press Enter to continue.", \
			max_x/2, (max_y/2)+100)
		self.Exit_mark = text(screen, 60, (240, 240, 240), "Press Escape to exit.", \
			max_x/2, (max_y/2)+180)
		self.mastec = image(screen, (max_x/2)-75, max_y/2-250,'images/mastec_vysmech.jpeg')


	def render_and_handle(self):
		pygame.time.delay(50)
		self.screen.fill(self.color)
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RETURN:
					return 'menu_1'
				elif event.key == pygame.K_ESCAPE:
					exit()
		else:
			self.Loss_mark.render()
			self.Continue_mark.render()
			self.Exit_mark.render()
			self.mastec.render()
		return 'stage_loss'	

if __name__ == "__main__":
	pass
