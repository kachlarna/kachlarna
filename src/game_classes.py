#!/usr/bin/env python3
# Authors:  David Vodák <xvodak05@stud.fit.vutbr.cz>
#           Adam Freiberg <xfreib00@stud.fit.vutbr.cz>

import pygame


######################### renderables ###############################
class renderable(object):
	""" Template class for rendering objects"""

	def __init__(self, screen, x, y):
		"""Renderable constructor

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's top left corner
		    y (int): x axis coordinate of object's top left corner
		"""
		self.screen = screen
		self.x = x
		self.y = y

	def render(self, x = 0, y = 0):
		"""Render method, needs to be overridden in every child class

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""				
		pass

	def move(self, right, down):
		"""Method for moving renderable object

		Args:
		    right (int): number of pixels object should be moved to the right
		    down (int): number of pixels object should be moved down
		"""		
		self.x += right
		self.y += down

class image(renderable):
	"""Class for rendering images
	
		Child class of renderable class

	"""	
	def __init__(self, screen, x, y, image_path):
		"""Image class constructor

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's top left corner
		    y (int): y axis coordinate of object's top left corner
		    image_path (str): path to image
		"""		
		super().__init__(screen, x, y)
		self.img = pygame.image.load(image_path)

	def render(self, x = 0, y = 0):
		"""Method for rendering image based on constructor parameters
			Overridden render method from rendable class

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""		
		self.screen.blit(self.img, (self.x + x, self.y + y))

class rectangle(renderable):
	"""Class for rendering rectangles
	
		Child class of renderable class

	"""
	def __init__(self, screen, x, y, width, length, color):
		"""Constructor of rectangle class

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's top left corner
		    y (int): y axis coordinate of object's top left corner
		    width (int): width of rectangle (number of pixels)
		    length (int): length of rectangle (number of pixels)
		    color (touple(R,G,B)): pygame's RGB touple representing color of rectangle
		"""		
		super().__init__(screen, x, y)
		self.width = width
		self.length = length
		self.color = color

	def render(self, x = 0, y = 0):
		"""Method for rendering rectangle based on constructor parameters
			Overridden render method from rendable class

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""		
		rect = pygame.Rect(self.x + x , self.y + y, self.width, self.length)
		pygame.draw.rect(self.screen, self.color, rect)

class line(renderable):
	"""Class for rendering line
	
		Child class of renderable class
	"""	
	def __init__(self, screen, x, y, end_x, end_y, width = 1, color = pygame.Color(0,0,0)):
		"""Constructor of line class

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's starting point
		    y (int): y axis coordinate of object's starting point
		    end_x (int): x axis coordinate of object's end point
		    end_y (int): y axis coordinate of object's end point
		    width (int, optional): width of line in pixels. Defaults to 1.
		    color (touple(R,G,B), optional): pygame's RGB touple representing color of line. Defaults to pygame.Color(0,0,0).
		"""		
		super().__init__(screen, x, y)
		self.end_x = end_x
		self.end_y = end_y
		self.width = width
		self.color = color

	def render(self, x = 0, y = 0):
		"""Method for rendering line based on constructor parameters
			Overridden render method from rendable class

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""		
		pygame.draw.line(self.screen, self.color, (self.x + x, self.y + y), \
				(self.end_x + x, self.end_y + y), self.width)

	def move(self, right, down):
		"""Method for moving line object
			Uses move from parent class while overridding it
		Args:
		    right (int): number of pixels object should be moved to the right
		    down (int): number of pixels object should be moved down
		"""		
		super().move(right, down)
		self.end_x += right
		self.end_y += down

class text(renderable):

	def __init__(self, screen, fontsize, colour, text, x, y):
		super().__init__(screen, x, y)
		font = pygame.font.Font(None, fontsize)
		self.textval = text
		self.text = font.render(text, 1, colour)

	def render(self, x = 0, y = 0):
		"""Method for rendering text

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""		
		textpos = self.text.get_rect(center=(self.x + x, self.y + y))
		self.screen.blit(self.text, textpos)

######################### hitboxes ###############################


class hitbox_template():
	"""Template for hitboxes to provide the same api"""
	def __init__(self, center_x, center_y):
		"""Constructor for hitboxes

		Args:
			center_x (int): position in x axis (center of hitbox)
			center_y (int): position in y axis (center of hitbox)
		"""
		pass

	def got_hit(self, forein_x, forein_y):
		"""Method to tell if the hitbox was hited by an object

		Returns True if the hitbox was hit by a foreint object,
		defined by forein_x and forein_y. Returns false otherwise

		Args:
			forein_x (int): position in x axis (forein object)
			forein_y (int): position in y axis (forein object)
		"""		
		pass

	def render(self, screen, x = 0, y = 0):
		"""Render borders of hitbox

		This method is used for debbuging, it should NOT be used
		in normal game mode.

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
			screen (surface): surface to be drawn into
			x (int): x axis coordinate
			y (int): y axis coordinate
		"""		
		pass

	def move(self, right, down):
		"""Method for moving hitbox object

		Args:
		    right (int): number of pixels object should be
		                 moved to the right
		    down (int): number of pixels object should be moved down
		"""
		self.center_x += right
		self.center_y += down


class rectangle_hitbox(hitbox_template):
	"""Rectangle-like hitbox

	Inherits from hitbox_template
	"""
	def __init__(self, center_x, center_y, height, width):
		"""Constructor fo rectangle_hitbox

		Args:
			center_x (int): position in x axis (center of hitbox)
			center_y (int): position in y axis (center of hitbox)
			height (int): height of the hitbox
			width (int): width of the hitbox
		"""
		self.half_height = height / 2
		self.half_width = width / 2
		self.center_x = center_x
		self.center_y = center_y

	def got_hit(self, forein_x, forein_y):
		"""Method to tell if the hitbox was hited by an object

		Returns True if the hitbox was hit by a foreint object,
		defined by forein_x and forein_y. Returns false otherwise

		Args:
			forein_x (int): position in x axis (forein object)
			forein_y (int): position in y axis (forein object)
		"""
		return ((forein_x >= self.center_x - self.half_width and
			forein_x <= self.center_x + self.half_width) and
			(forein_y >= self.center_y - self.half_height and
			forein_y <= self.center_y + self.half_height))

	def render(self, screen, x = 0, y = 0):
		"""Render borders of hitbox

		This method is used for debbuging, it should NOT be used
		in normal game mode.


		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
			screen (surface): surface to be drawn into
			x (int): x axis coordinate
			y (int): y axis coordinate
		"""	
		# top left corner
		a = (self.center_x - self.half_width + x, \
				self.center_y - self.half_height + y)
		# bottom left corner
		b = (self.center_x - self.half_width + x, \
				self.center_y + self.half_height + y)
		# top right corner
		c = (self.center_x + self.half_width + x, \
				self.center_y - self.half_height + y)
		# bottom right corner
		d = (self.center_x + self.half_width + x, \
				self.center_y + self.half_height + y)

		pygame.draw.line(screen, pygame.Color(150, 255, 150), a, b, 3)
		pygame.draw.line(screen, pygame.Color(150, 255, 150), c, d, 3)
		pygame.draw.line(screen, pygame.Color(150, 255, 150), a, c, 3)
		pygame.draw.line(screen, pygame.Color(150, 255, 150), b, d, 3)

######################### game objects ###############################

class game_object(object):
	"""Template game_object class"""
		
	def __init__(self, screen, x, y):
		"""Game_object class constructor

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's center
		    y (int): x axis coordinate of object's center
		"""	
		self.screen = screen
		self.x = x
		self.y = y
		self.render_objs = []
		self.hitbox_objs = []

	def render(self, x = 0, y = 0):
		"""Method used for rendering objects on screen

		The optional axis coordinates are used in stages, where rendering
		objects should be dependent on coordinates. This is done to provide
		more space to the game than just a size of window.

		Args:
		    x (int): x axis coordinate
		    y (int): y axis coordinate
		"""		
		for render_obj in self.render_objs:
			render_obj.render(x, y)

	def got_hit(self, forein_x, forein_y):
		"""Method to tell if the hitbox_objs were hited by an object

		Returns True if the hitbox was hit by a foreint object,
		defined by forein_x and forein_y. Returns false otherwise

		Args:
			forein_x (int): position in x axis (forein object)
			forein_y (int): position in y axis (forein object)
		"""		
		for hitbox_obj in self.hitbox_objs:
			if hitbox_obj.got_hit(forein_x, forein_y):
				return True
		return False

class movable(game_object):
	"""Class for movable objects
		Child class of game_object
	"""
	def __init__(self, screen, x, y):
		"""Movable class constructor

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of object's center
		    y (int): x axis coordinate of object's center
		"""		
		super().__init__(screen, x, y)

	def move(self, right, down):
		"""Move the object

		Args:
		    right (int): number of pixels object should be moved to the right
		    down (int): number of pixels object should be moved down
		"""
		self.x += right
		self.y += down
		for render_obj in self.render_objs:
			render_obj.move(right, down)
		for hitbox_obj in self.hitbox_objs:
			hitbox_obj.move(right, down)

class dan(movable):

	def __init__(self, screen, x, y):
		super().__init__(screen, x, y)
		self.head_right = image(screen, x - 20, y - 60, 'images/dan_2_right.png')
		self.head_left  = image(screen, x - 35, y - 60, 'images/dan_2_left.png')
		body = rectangle(screen, x - 15, y, 30, 50, \
			(20, 20, 20))
		left_arm = line(screen, x - 13, y, x - 60, y - 30, width = 10)
		right_arm = line(screen, x + 13, y, x + 60, y - 30, width = 10)

		left_leg = line(screen, x - 10, y + 45, x - 15, y + 90, width = 10)
		right_leg = line(screen, x + 10, y + 45, x + 15, y + 90, width = 10)

		hitbox = rectangle_hitbox(x, y, 140, 70)

		self.render_objs.append(body)
		self.render_objs.append(left_arm)
		self.render_objs.append(right_arm)
		self.render_objs.append(left_leg)
		self.render_objs.append(right_leg)
		self.render_objs.append(self.head_right)

		self.hitbox_objs.append(hitbox)

	def move(self, right, down):
		"""Method for moving dan object
			Uses move from parent class while overridding it
		Args:
		    right (int): number of pixels object should be moved to the right
		    down (int): number of pixels object should be moved down
		"""		
		super().move(right, down)
		if self.head_left in self.render_objs:
			self.head_right.move(right, down)
		else:
			self.head_left.move(right, down)

	def head_points_left(self):
		if self.head_right in self.render_objs:
			self.render_objs.remove(self.head_right)
			self.render_objs.append(self.head_left)

	def head_points_right(self):
		if self.head_left in self.render_objs:
			self.render_objs.remove(self.head_left)
			self.render_objs.append(self.head_right)


class TheDan(dan):
	def __init__(self, screen, x, y, stepables, lava, win_place):
		"""TheDan class constructor

		Args:
		    screen (surface): surface to be drawn into
		    x (int): x axis coordinate of TheDan's center
		    y (int): x axis coordinate of TheDan's center
		    stepables:   list of objects that inherits from game_object
		    lava:        lava object
		    win_place:   object that inherits from game_object, if dan
		                 steps to that place he wins
		"""
		super().__init__(screen, x, y)
		self.jump_cnt = 0
		self.jump_speed = 30
		self.fall_speed = 10
		self.speed = 8
		self.stepables = stepables
		self.lava = lava
		self.win_place = win_place

	def jump(self):
		if not self.__has_to_fall():
			self.jump_cnt = 20

	def run_left(self):
		self.move(-self.speed, 0)
		self.head_points_left()

	def run_right(self):
		self.move(self.speed, 0)
		self.head_points_right()
		
	def live(self):
		"""Dan LIVES yaay

		This function checks and reacts to all events that might
		happen to dan.

		Returns string which describes the state of the game:
			"l" - loss      (Dan died >,<)
			"w" - win       (Dan won)
			"c" - carry on  (Dan didnt actualy do anything
					 interesting. It happens most of
					 the time he is kinda boring)
			"v" - void      (Dan has fallen into the void,
			                 what a dumbass)
		"""
		if self.jump_cnt > 0:
			self.move(0, - self.jump_speed)
			self.jump_cnt -= 1
			self.jump_speed -= 1
		else:
			self.jump_speed = 30

		if self.__has_to_fall() and self.jump_cnt == 0:
			self.move(0, self.fall_speed)
			self.fall_speed += 1
		else:
			self.fall_speed = 10

		if self.__swimmed_in_lava():
			return "l"

		if self.__did_it():
			return "w"
	
		if self.y > 3000:
			return "v"

		return "c"


	def __has_to_fall(self):
		for kachle in self.stepables:
			if kachle.got_hit(self.x + 15, self.y + 90):
				return False
			if kachle.got_hit(self.x - 15, self.y + 90):
				return False
		return True

	def __swimmed_in_lava(self):
		return self.lava.got_hit(self.x, self.y + 90)

	def __did_it(self):
		return self.win_place.got_hit(self.x, self.y + 90)

class kachle(game_object):

	def __init__(self, screen, x, y, size):
		super().__init__(screen, x, y)

		rect1 = rectangle(screen, x + 5 - size/2, y + 5 - size/2, \
			size - 9, size - 9, (102, 51, 0))
		rect2 = rectangle(screen, x - size/2, y - size/2, \
			size, size, (0, 0, 0))

		line1 = line(screen, x - size/2, y, \
			 x - 5 + size/2, y, width = 5)
		line2 = line(screen, x, y - size/2, \
			 x, y - 5 + size/2, width = 5)

		hitbox = rectangle_hitbox(x, y, size, size)

		self.render_objs.append(rect2)
		self.render_objs.append(rect1)
		self.render_objs.append(line1)
		self.render_objs.append(line2)

		self.hitbox_objs.append(hitbox)

class lava(movable):

	def __init__(self, screen, x, y, width, length):
		super().__init__(screen, x, y)

		rect = rectangle(screen, x - width/2, y - length/2, \
			width, length, (255, 51, 0))
		
		crust = line(screen, x - width/2, y - length/2, x - 1 + width/2, \
			y - length/2, 3, (255, 153, 51))

		hitbox = rectangle_hitbox(x, y, length, width)

		self.render_objs.append(rect)
		self.render_objs.append(crust)
		self.hitbox_objs.append(hitbox)
		self.cnt = 0

	def grow(self):
		self.move(0, -1)
		self.cnt = self.cnt + 1

	def get_back(self):
		self.move(0, self.cnt)
		self.cnt = 0	


if __name__ == "__main__":
	pass
