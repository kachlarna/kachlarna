#!/usr/bin/env python3

# Authors:  David Vodák <xvodak05@stud.fit.vutbr.cz>
#           Adam Freiberg <xfreib00@stud.fit.vutbr.cz>

import pygame
from stages import menu_1, stage_1, stage_2, stage_win, stage_loss, stage_void

class Kachlarna:
	"""Class for running application's main window"""

	def __init__(self, max_x, max_y):
		""" Kachlarna constructor

		Args:
			max_x (int): Number of pixels in the x axis
			max_y (int): Number of pixels in the y axis
		"""		
		pygame.init()
		self.running = True
		self.screen = pygame.display.set_mode((max_x, max_y))
		
		"""Stages registration
		
		Every stage (see stages.py) should be here, otherwise this stage
		wont be able to run render_and_handle() method inside kachlarna
		and therefore will be completely useless.

		If you just added a new stage just register it here by adding
		'stage_xyz' : stage_xyz(self.screen, max_x, max_y)
		to the game_stages dictionary.

		NOTE that key in the dictionary must be equal to the name of a
		stages class!
		"""		
		self.game_stages = {
			'menu_1'    :     menu_1(self.screen, max_x, max_y),
			'stage_1'   :    stage_1(self.screen, max_x, max_y),
			'stage_2'   :    stage_2(self.screen, max_x, max_y),
			'stage_win' :  stage_win(self.screen, max_x, max_y),
			'stage_loss': stage_loss(self.screen, max_x, max_y),
			'stage_void': stage_void(self.screen, max_x, max_y)
		}
		self.current_stage = 'menu_1'
		pygame.display.set_caption('Kachlárna')

	def run(self):
		"""Infinite loop
		
		This function just call stage.render_and_handle() method,
		for more see stages.py
		"""		
		while self.running:
			self.current_stage = self.game_stages[self.current_stage].render_and_handle()
			pygame.display.update()


if __name__ == "__main__":
    pass
