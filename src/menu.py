#!/usr/bin/env python3 
# Author:  Kamil Vojanec <xvojan00@stud.fit.vutbr.cz>

import pygame
from game_classes import text, rectangle

class textMenu():
	""" 
	Game menu class. Draws a menu on top of given screen. Menu elements are
	passed as a list of strings. To move the cursor, use move_up() and move_down()
	methods.
	"""
	def __init__(self, screen,
		     elemlist: list,
		     fontsize: int = 40,
		     color: (int, int, int)=(200, 200, 200),
		     highlight: (int, int, int)=(50, 10, 10),
		     padding_top: int = 20):
		self.screen = screen
		self.fontsize = fontsize
		self.color = color
		self.highlight = highlight
		self.padding_top = padding_top
		self.elem_strings = elemlist
		self._prepare_menu()

	def _prepare_menu(self):
		"""
		Prepare all menu elements. Menu consists of 3 lists:
		unselected: Contains styles of unselected elements
		selected:   Contains styles of selected elements
		actual:     Combines one selected item and other unselected items
		"""
		self.unselected = [
			text(
				self.screen,
				self.fontsize,
				self.color,
				line,
				self.screen.get_rect().centerx,
				i*self.fontsize+self.padding_top
			) for i, line in enumerate(self.elem_strings)]

		self.selected = [
			text(
				self.screen,
				self.fontsize,
				self.highlight,
				line,
				self.screen.get_rect().centerx,
				i*self.fontsize+self.padding_top
			) for i, line in enumerate(self.elem_strings)]

		self.idx = 0
		self.actual = [self.selected[self.idx]] + self.unselected[self.idx+1:]

	def _move(self):
		self.actual = self.unselected[:self.idx] + [self.selected[self.idx]] + self.unselected[self.idx+1:]

	def move_down(self):
		"""
		Move currently selected item one field down
		"""
		if self.idx == len(self.actual)-1:
			self.idx = 0
		else:
			self.idx += 1
		self._move()

	def move_up(self):
		"""
		Move currently selected item one field up
		"""
		if self.idx == 0:
			self.idx = len(self.actual)-1
		else:
			self.idx -= 1
		self._move()

	def select(self):
		"""
		Return selected item
		"""
		return self.actual[self.idx]

	def render(self):
		"""
		Render all menu items
		"""
		for i in self.actual:
			i.render()

class menuBox(rectangle):
	"""
	Single box with text to be used in boxMenu class
	"""
	def __init__(self, screen, x, y, width, length, color, item, fontsize=20, textcolor=(200, 200, 200)):
		super().__init__(screen, x, y, width, length, color)
		text_x = x + width // 2
		text_y = y + length // 2
		self.textval = item
		self.menu_item = text(screen, fontsize, textcolor, item, text_x, text_y)
		
	def render(self):
		super().render()
		self.menu_item.render()

class boxMenu():
	""" 
	Game menu class. Draws a menu on top of given screen. Menu elements are
	passed as a list of strings. To move the cursor, use move_up() and move_down()
	methods.
	"""
	def __init__(self, screen,
		     elemlist: list,
		     fontsize: int = 40,
		     color: (int, int, int)=(200, 200, 200),
		     highlight: (int, int, int)=(50, 10, 10),
		     padding_top: int = 20):
		self.screen = screen
		self.fontsize = fontsize
		self.color = color
		self.highlight = highlight
		self.padding_top = padding_top
		self.elem_strings = elemlist
		self._prepare_menu()


	def _prepare_menu(self):
		"""
		Prepare a boxMenu from three lists. See textMenu class for more details
		"""
		height = self.screen.get_height() // len(self.elem_strings)
		self.unselected = [
			menuBox(
				self.screen,
				0,
				i*height,
				self.screen.get_width(),
				height,
				self.highlight,
				line,
				self.fontsize,
				self.color
			) for i, line in enumerate(self.elem_strings)]
		self.selected = [
			menuBox(
				self.screen,
				0,
				i*height,
				self.screen.get_width(),
				height,
				self.color,
				line,
				self.fontsize,
				self.highlight
			) for i, line in enumerate(self.elem_strings)]


		self.idx = 0
		self.actual = [self.selected[self.idx]] + self.unselected[self.idx+1:]

	def _move(self):
		self.actual = self.unselected[:self.idx] + [self.selected[self.idx]] + self.unselected[self.idx+1:]

	def move_down(self):
		"""
		Move selected item one space down
		"""
		if self.idx == len(self.actual)-1:
			self.idx = 0
		else:
			self.idx += 1
		self._move()

	def move_up(self):
		"""
		Move selected item one space up
		"""
		if self.idx == 0:
			self.idx = len(self.actual)-1
		else:
			self.idx -= 1
		self._move()

	def select(self):
		"""
		Return selected item
		"""
		return self.actual[self.idx]

	def render(self):
		"""
		Render all menuBoxes as one menu
		"""
		for i in self.actual:
			i.render()

